<?php

namespace MOOC\framework;

use Exception;

class FrontController extends FrontControllerAbstract
{
    public function __construct()
    {
        //forces you to use the run method
    }

    private function __clone()
    {

    }

    public static function run()
    {
        $controller = new FrontController();
        $controller->init();
        $controller->handleRequest();

       
    }

    /**
     * Use this method to initialize helper objects
     * Session Manager, Validator, Response Handler, etc
     * They should be globally available objects. Initialize them here so they can be used elsewhere.
     */
    protected function init()
    {
        
        $registry = Registry::instance();
        $session = $registry->getSession();
      
        $validate = $registry->getValidation();
        
        $response = $registry->getHandler();
      
    }

    protected function handleRequest()
    {   error_reporting (E_ALL ^ E_NOTICE);
       
        $page = "";

        $context = new CommandContext();
        
        $middle = $context->get('get');
        //var_dump($middle);
        $newRequest = $middle['controller'];
       

        if(empty($newRequest))
        {
            $page = 'index';
        }

        else
        {
            $page = $newRequest;
        }
        

        $handler = RequestHandlerFactory::makeRequestHandler($page);
        
        echo "<br>";
          
        $registry = Registry::instance();
        $session = $registry->getSession();

        $response = $registry->getHandler();
     
        $session->create();


        if($handler->execute($context) === false)
        {
            //echo "Error! Incorrect Request Passed!";
            
            $head =  new WarningHeader();
            $state = new WarningState();
            $logger = new WarningLogger();

            $set = array("HTTP Status 400");
            $head->setEntries($set);

            $set = array("You sent an invalid request using the URL. Please ensure that the ' ?controller= ' part of the URL is valid. ");
            $state->setEntries($set);

            $time = date("h:i:sa");
            $date = date("Y-m-d");
    
            $fulltime = "[ ". $date. " ". $time . " ]";
    
            $set = array($fulltime);
            
            $response->create($head, $state, $logger);

            $session->add("HEADER", $head->getEntry(0));
            $session->add("STATE", $state->getEntry(0));
            $session->add("LOGGER", $logger->getEntry(0));

        }
        
        $session->add("RESPONSE", $response);

    
        return true;
    }

}



