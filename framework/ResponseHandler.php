<?php

namespace MOOC\framework;

class ResponseHandler extends Singleton_Class implements ResponseHandlerInterface 
{
    protected $body = [];

    public function __construct()
    {
        
    }


    public function create(ResponseHeader $head, ResponseState $state, ResponseLogger $logger)
    {
        $this->body['header'] = $head;
        $this->body['state'] = $state;
        $this->body['logger'] = $logger;

        $this->writeToFile();
    }

     // returns response header
     public function giveHeader(): ResponseHeader
     {
         return clone $this->body['header'];
     }

     // returns response state
    public function giveState() : ResponseState
    {
        return clone $this->body['state'];
    }

     // returns response logger
    public function giveLogger() : ResponseLogger
    {
        return clone $this->body['logger'];
    }


    public function writeToFile()
    {
        $testhead = $this->giveHeader();
        $headtext = $testhead->getEntry(0);
        //var_dump($headtext); echo "<br><br>";

        $teststate = $this->giveState();
        $statetext = $teststate->getEntry(0); echo "<br><br>";

        $testlogger = $this->giveLogger();
        $loggertext = $testlogger->getEntry(0); echo "<br><br>";
       

        if(file_exists(DATA_DIR . '/ResponseLog.txt'))
        {
            $myfile = fopen((DATA_DIR . '/ResponseLog.txt'), 'a');
            fwrite($myfile, $headtext);
            fwrite($myfile, "\r\n");
            
            fwrite($myfile, $statetext);
            fwrite($myfile, "\r\n");

            fwrite($myfile, "Logged at ");
            fwrite($myfile, $loggertext);
            fwrite($myfile, "\r\n");

            fwrite($myfile, "___________________________________________________________________________");

            fwrite($myfile, "\r\n\r\n");

            fclose($myfile);
        }
    }



}