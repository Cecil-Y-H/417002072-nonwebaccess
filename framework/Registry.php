<?php

namespace MOOC\framework;

use mysqli;
use Exception;



class Registry
{
    private static $instance = null;
    
    private $session = null;
    private $validate =  null;
    private $handler = null;

    private static $connection = null;
    
    public static function instance(): self
    {
        if(is_null(self::$instance))
        {
            self::$instance = new self();
        }

        return self::$instance;
    }

    public function getSession(): SessionClass
    {
        if(is_null($this->session))
        {
            $this->session = SessionClass::getInstance();
        }

        return $this->session;
    }
   
    public function getValidation(): Validation
    {
        if(is_null($this->validate))
        {
            $this->validate = Validation::getInstance();
        }

        return $this->validate;
    }


    public function getHandler(): ResponseHandler
    {
        if(is_null($this->handler))
        {
            $this->handler = ResponseHandler::getInstance();
        }

        return $this->handler;
    }

    /**
     * Since we're using one database and the credientials never change.
     * We are defining the credentials inside the function.
     */
    public function getConnection(): mysqli
    {
        if(is_null(self::$connection))
        {
            $dbhost = "localhost";
            $dbuser = "root";
            $dbpass = '';
            $db = "mooc";
    
            
            self::$connection = new mysqli($dbhost, $dbuser, $dbpass, $db);

            if(self::$connection->connect_error)
            {
                $response = $this->getHandler();
                $session = $this->getSession();
                $head =  new WarningHeader();
                $state = new WarningState();
                $logger = new WarningLogger();

                $set = array("HTTP Status 500");
                $head->setEntries($set);

                $set = array("There was an error connecting to the Database on the Server-Side. Please ensure you either have a proper connection with the correct credentials or that the Database itself is present at the destination.");
                $state->setEntries($set);

                $time = date("h:i:sa");
                $date = date("Y-m-d");

                $fulltime = "[ ". $date. " ". $time . " ]";
               
                $set = array($fulltime);
                $logger->setEntries($set);
        
                $response->create($head, $state, $logger);

                $session->add("HEADER", $head->getEntry(0));
                $session->add("STATE", $state->getEntry(0));
                $session->add("LOGGER", $logger->getEntry(0));


                try
                {
                    throw new Exception("ERROR: Please check ResponseLog.txt in data directory for further details");  
                }
                 
                
                catch(Exception $e)
                {

                    echo 'Message: ' .$e->getMessage();
                    die();
                }

            }
            

            return self::$connection;
        }

        return self::$connection;
    }



   
}