<?php

namespace MOOC\framework;
use MOOC\framework\Registry;

use mysqli;

class ProfileMapper extends MapperAbstract
{
    public function read()
    {
       
        //var_dump($this->adapter->data);

        $connect = $this->connection;

        $loggedEmail = $this->adapter->data[0];

        $table1 = "select courses.course_id, courses.course_name, courses.course_image, faculty_department.faculty_dept_name, instructors.instructor_name from faculty_department, faculty_dept_courses, courses, course_instructor, instructors where courses.course_id = faculty_dept_courses.course_id AND faculty_dept_courses.faculty_dept_id = faculty_department.faculty_dept_id AND courses.course_id = course_instructor.course_id AND course_instructor.course_id = course_instructor.instructor_id AND course_instructor.instructor_id = instructors.instructor_id";

		$table2 = "SELECT * from user_courses WHERE email='$loggedEmail'";

		$result = $connect->query($table2);
		$studentCourses = [];

		if($result->num_rows > 0)
		{
			
			while($row = $result->fetch_assoc())
			{
				array_push($studentCourses, $row['course_id']);
				
			}
		}
	
		//var_dump($studentCourses);

		$result = $connect->query($table1);

		$courseName = [];
		$courseImage = [];
		$faculty = [];
		$instructor = [];

		if($result->num_rows > 0)
		{
			//$x = 0;
			while($row = $result->fetch_assoc())
			{	
				//echo $row['course_id'];
				//echo "<br>";

				if( 
					($studentCourses[0] == $row['course_id']) ||
					($studentCourses[1] == $row['course_id']) ||
					($studentCourses[2] == $row['course_id']) ||
					($studentCourses[3] == $row['course_id']) 
				)
				{
					//echo "Matching Course Found!";
					//echo "<br>";

					array_push($courseName, $row["course_name"]);
					array_push($courseImage, $row["course_image"]);
					array_push($faculty, $row["faculty_dept_name"]);
					array_push($instructor, $row["instructor_name"]);
					//$x++;
				}	
				
				
			}
		}

		//var_dump($courseImage);

		
		$result = [			
			'Name'=>$courseName,
			'Image'=>$courseImage,
			'Faculty'=>$faculty,
			'Instructor'=>$instructor
        ];	
        
        $this->adapter->populateData($result);

        return $this->adapter;
    }
}