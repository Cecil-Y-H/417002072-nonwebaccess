<?php

namespace MOOC\framework;




class IndexMapper extends MapperAbstract
{
   
    public function read()
    {
       
       $connect = $this->connection;
        
        //echo "Index Mapper Map All Records Function Invoked!";


        $POPULAR = "select courses.course_name, courses.course_image, instructors.instructor_name, courses.course_access_count from courses, course_instructor, instructors where courses.course_id = course_instructor.course_id and course_instructor.course_id = course_instructor.instructor_id and course_instructor.instructor_id = instructors.instructor_id ORDER BY courses.course_access_count DESC";
        
        $result = $connect->query($POPULAR);

        $popCourse = [];
        $popInstruct = [];
        $popImage = [];
        

        //var_dump($result);
        if($result->num_rows > 0)
        {
            while($row = $result->fetch_assoc())
            {
            
                array_push($popCourse, $row["course_name"]);
                array_push($popInstruct, $row["instructor_name"]);
                array_push($popImage, $row["course_image"]);

            }
        }


        $popCourse = array_slice($popCourse, 0, 8);
        $popInstruct = array_slice($popInstruct, 0, 8);
        $popImage = array_slice($popImage, 0, 8);





        $RECOMMENDED = "select courses.course_name, courses.course_image, instructors.instructor_name, courses.course_recommendation_count from courses, course_instructor, instructors where courses.course_id = course_instructor.course_id and course_instructor.course_id = course_instructor.instructor_id and course_instructor.instructor_id = instructors.instructor_id ORDER BY courses.course_recommendation_count DESC";
        
        $result = $connect->query($RECOMMENDED);

        $RecomCourse = [];
        $RecomInstruct = [];
        $RecomImage = [];

        if($result->num_rows > 0)
        {
            while($row = $result->fetch_assoc())
            {
            
                array_push($RecomCourse, $row["course_name"]);
                array_push($RecomInstruct, $row["instructor_name"]);
                array_push($RecomImage, $row["course_image"]);

            }
        }

        $RecomCourse = array_slice($RecomCourse, 0, 8);
        $RecomInstruct = array_slice($RecomInstruct, 0, 8);
        $RecomImage = array_slice($RecomImage, 0, 8);








        $returnArray =  [   'PopCourse'=>$popCourse,
                            'PopInstruct'=>$popInstruct,
                            'PopImage'=>$popImage,

                            'RecomCourse'=>$RecomCourse,
                            'RecomInstruct'=>$RecomInstruct,
                            'RecomImage'=>$RecomImage
                         ];
       

        $this->adapter->populateData($returnArray);
        

        /**
         * Returning the Storage Adapter Object with the data retrieved from the database
         */
        return $this->adapter;
    }


}