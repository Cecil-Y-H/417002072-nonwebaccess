<?php

namespace MOOC\framework;

interface RequestHandlerFactoryInterface
{
    //public static function makeRequestHandler(string $request='default') : PageControllerCommandAbstract;
    
    public static function makeRequestHandler(string $request) : PageControllerCommandAbstract;
}