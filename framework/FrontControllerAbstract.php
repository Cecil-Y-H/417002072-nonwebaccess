<?php

namespace MOOC\framework;

abstract class FrontControllerAbstract
{

    //Stores Request Handler Object
    protected $reqHandler = null;


    //Invokes the controller itself
    abstract public static function run();


    //Initializes the controller with a framework
    //Validator, Session Manager and Response Handler needed as 
    //helper classes among others
    abstract protected function init();


    /**
     * Takes the data passed to the front controller from 
     * the GET or POST methods, checks them and then passes
     * them on to the request handler for processing
    */
    abstract protected function handleRequest();




}