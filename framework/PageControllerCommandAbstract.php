<?php

namespace MOOC\framework;

abstract class PageControllerCommandAbstract implements CommandInterface
{
    protected $model = null;
    protected $view = null;

    /*
    public function setModel(ObservableModel $model)
    {

        $this->model = $model;
    }*/

    //Modified setModel Method to work with the Model Factory.
    //Takes a string describring what kind of model is requested
    //And creates that type of model as an object
    abstract public function CreateModel() : ObservableModel;
    
    abstract public function CreateView() : View;

    /**
     * The run method has been modified to take a variable from the Command Context which indicates which page it requested.
     */

    abstract public function run(string $request);
    
	abstract public function execute(CommandContext $context) : bool;
	
	
	//Please note that these two methods are STRICTLY for testing purposes only.
	//They do not factor into the use of the framework proper.
	
	public function getModel()
	{
		return $this->model;
	}
	
	public function getView()
	{
		return $this->view;
	}
}

