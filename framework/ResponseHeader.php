<?php

namespace MOOC\framework;

class ResponseHeader extends ResponseAbstract
{
    //protected $data = [];

    // addEntries in the videos
    public function setEntries(array $entries) : bool
    {
        array_push($this->data, $entries);
        //echo sizeof($this->data); 
        //var_dump($this->data); echo "<br><br>";

        return true;
    }

    // showEntry in the videos.
    public function getEntry(int $i) : string
    {
        $result = $this->data[$i];
        return $result[0];
    }

    // showEntries in the videos
    public function getEntries(int $start, int $end) : string
    {
        for($x = $start; $x <= $end; $x++)
        {
            $result = $this->data[$x];
            echo $result[0];
            echo "<br><br>";
        }
        return '';
    }


}


