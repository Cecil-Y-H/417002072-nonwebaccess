<?php

namespace MOOC\framework;




class CoursesMapper extends MapperAbstract
{
    public function read()
    {
        $connect = $this->connection;

		$join = "select courses.course_name, courses.course_image, faculty_department.faculty_dept_name, instructors.instructor_name from faculty_department, faculty_dept_courses, courses, course_instructor, instructors where courses.course_id = faculty_dept_courses.course_id AND faculty_dept_courses.faculty_dept_id = faculty_department.faculty_dept_id AND courses.course_id = course_instructor.course_id AND course_instructor.course_id = course_instructor.instructor_id AND course_instructor.instructor_id = instructors.instructor_id";

		// course_name
		// course_image
		// faculty_dept_name
		// instructor_name

		$result = $connect->query($join);

		$courseName = [];
		$courseImage = [];
		$faculty = [];
		$instructor = [];

		$ResultArray = [];

		if($result->num_rows > 0)
		{
			while($row = $result->fetch_assoc())
			{
				/*
				echo $row["course_name"];
				echo "<br>";
				echo $row["course_image"];
				echo "<br>";
				echo $row["faculty_dept_name"];
				echo "<br>";
				echo $row["instructor_name"];
				echo "<br><br>";
				*/

				array_push($courseName, $row["course_name"]);
				array_push($courseImage, $row["course_image"]);
				array_push($faculty, $row["faculty_dept_name"]);
				array_push($instructor, $row["instructor_name"]);

				
			}
		}				


		
		$returnArray = [			
			'Name'=>$courseName,
			'Image'=>$courseImage,
			'Faculty'=>$faculty,
			'Instructor'=>$instructor
        ];
        
        $this->adapter->populateData($returnArray);

        return $this->adapter;
    }
}