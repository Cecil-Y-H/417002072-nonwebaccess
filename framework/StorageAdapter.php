<?php

namespace MOOC\framework;

class StorageAdapter
{
    public array $data = [];

    public function __construct(array $data)
    {
        $this->data = $data;
    }

    /**
     * Populates the Storage Adapter Object with data retrieved from the database.
     */

    public function populateData(array $retrieved)
    {
        //echo "Populate Data Method Invoked!";
        $this->data = $retrieved;

        //print_r($this->data);
    }


    
    public function returnAdapter()
    {
        return $this->data;
    }











    public function find(int $id)
    {
        if(isset($this->data[$id]))
        {
            return $this->data[$id];
        }

        return null;
    }
}