<?php

namespace MOOC\framework;
use MOOC\framework\Registry;

use mysqli;

abstract class MapperAbstract
{
    /**
     * This object is passed between Model and Mapper and contains data from the database.
     */
    protected StorageAdapter $adapter;

    /**
     * The connection to the database created via Registry
     */
    public  $connection;

    public function __construct(StorageAdapter $storage)
    {

        $this->adapter = $storage;
        
        $registry = Registry::instance();
        $this->connection = $registry->getConnection();

       

    }
}