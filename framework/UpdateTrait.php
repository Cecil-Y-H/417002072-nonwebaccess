<?php
namespace MOOC\framework;

trait UpdateTrait
{
    abstract public function update(array $values);
}