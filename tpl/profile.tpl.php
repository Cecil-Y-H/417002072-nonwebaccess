<!DOCTYPE html>
<html lang="en-GB">
	<head>
		<title>Quwius</title>
		<link rel="stylesheet" href="css/styles.css" type="text/css" media="screen">
		<meta charset="utf-8">
	</head>
	<body>
		<nav> 
			<a href="index.php?controller=index"><img src="images/logo.png" alt="UWI online"></a>
			<ul>
				<!-- <li><a href="index.php">Test-Index</a></li> -->
				<li><a href="index.php?controller=courses">Courses</a></li>
				<li><a href="index.php?controller=profile">Streams</a></li>
				<li><a href="index.php?controller=profile">About Us</a></li>
				<li><a href="index.php?controller=logout">Log Out</a></li>
				<li><a href="index.php?controller=signup">Sign Up</a></li>
			
			</ul>
		</nav>

		<?php
			//echo $_SESSION['user'];
			//echo $_SESSION['LoggedIn'];
			//echo $user[0];
			//echo $user[1];
			//echo sizeof($user);

			//var_dump($Name);
		?>



		<main>
		<h1>Profile Page</h1>
		<h2>Enrolled Courses</h2>
		<ul class="course-list">
			
		
		<?php for($i = 0; $i < sizeof($Name) ; $i++) : ?>
			
			<li><div>
				<a href="#">
				<img src="images/<?php echo $Image[$i]?>" alt="<?php echo $Name[$i]; ?>"></a>
				</div>
				<div>
				<a href="#">
				<span class="faculty-department">
					<?php 
						echo $Faculty[$i]; 
					?>
				</span>	
					<span class="course-title">
							<?php echo $Name[$i]; ?>
					</span>
					<span class="instructor">
						<b> <?php 
						echo $Instructor[$i]; 
						
						?> </b>
					</span></a>
				</div>
				<div>
					<a href="#" class="startnow-btn startnow-button">Go to Class!</a>
					<a href="#" class="startnow-btn unenroll-button">Unenroll</a>
				</div>
			</li>
			
			
			<?php endfor; ?>
			
			
			
			
			
			
			
		</ul>
			<footer>
				<nav>
					<ul>
						<li>&copy;2015 Quwius Inc.</li>
						<li><a href="#">Company</a></li>
						<li><a href="#">Connect</a></li>
						<li><a href="#">Terms &amp; Conditions</a></li>
					</ul>
				</nav>
			</footer>
		</main>
	</body>
</html>