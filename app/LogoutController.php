<?php

namespace MOOC\apps;

use MOOC\framework\CommandContext;
use MOOC\framework\View;
use MOOC\framework\ObservableModel;
use MOOC\framework\PageControllerCommandAbstract;
use MOOC\framework\SessionClass;
use MOOC\framework\Registry;

use MOOC\framework\NoticeHeader;
use MOOC\framework\NoticeState;
use MOOC\framework\NoticeLogger;


/**
 * This controller is here to invoke the Log Out functionality using the Front Controller approach. Models and Views are only created to satisfy the abstract implementation of the PageControllerCommandAbstract
 */

class LogoutController extends PageControllerCommandAbstract
{
    public function run(string $request)
    {
        echo "Logout Functionality Activated"; echo "<br><br>";
        $sess = SessionClass::getInstance();
        $sess->create();
        $user = $sess->see('LoggedIn');
        echo $user;

        $registry = Registry::instance();
        $response = $registry->getHandler();

            $head = new NoticeHeader();
            $state = new NoticeState();
            $logger = new NoticeLogger();

            $set = array("HTTP Status 202");
            $head->setEntries($set);

            $set = array("User Logged Out! Logged In User has successfully logged out of the system. Now returning to Index Page");
            $state->setEntries($set);
        
            $time = date("h:i:sa");
			$date = date("Y-m-d");
	
			$fulltime = "[ ". $date. " ". $time . " ]";
	
			$set = array($fulltime);
			$logger->setEntries($set);

            $response->create($head, $state, $logger);
            $sess->add("RESPONSE", $response);


        $sess->destroy();

        header('Location:index.php');
       

    }

    
    public function CreateModel() : ObservableModel
	{
		return new IndexModel();
	}

	public function CreateView() : View
	{
		
		return new View();
	}


    public function execute(CommandContext $context) : bool
    {
        $contextData = $context->get('get');
        $newRequest =  $contextData['controller'];

        $this->run($newRequest);
        return true;
    }
}
