<?php

namespace MOOC\apps;

use MOOC\framework\CommandContext;
use MOOC\framework\PageControllerCommandAbstract;
use MOOC\framework\View;
use MOOC\framework\ObservableModel;
use MOOC\framework\Registry;

use MOOC\framework\NoticeHeader;
use MOOC\framework\NoticeState;
use MOOC\framework\NoticeLogger;
use MOOC\framework\WarningHeader;
use MOOC\framework\WarningState;
use MOOC\framework\WarningLogger;


class LoginController extends PageControllerCommandAbstract
{
	public function run(string $request)
	{
		//error_reporting(0);

		$registry = Registry::instance();

		$this->model = $this->CreateModel();

		$this->view = $this->CreateView();
		
		$this->model->attach($this->view);
		
		$data = array();
		
		$this->model->updateThechangedData($data);
		
		$this->model->notify();
		


		

			$response = $registry->getHandler();
			
			$session = $registry->getSession();
			
			$head = new NoticeHeader();
			$state = new NoticeState();
			$logger = new NoticeLogger();

			$set = array("HTTP Status 200");
			$head->setEntries($set);
	
			$set = array("Page Access OK. The Login Page was successfully accessed and displayed. Everything is ok.");
			$state->setEntries($set);
			
			$time = date("h:i:sa");
			$date = date("Y-m-d");
	
			$fulltime = "[ ". $date. " ". $time . " ]";
	
			$set = array($fulltime);
			$logger->setEntries($set);
	
			$response->create($head, $state, $logger);
			$session->add("RESPONSE", $response);

		if(!empty($_POST))
		{
			
			$validator = $registry->getValidation();
			$validator->fillData($_POST);
			$result = $validator->loginValidate();
			
			
			if(!$result)
			{
				echo "<br>";
				echo "Please see your Errors below!\n";
				$errors = $validator->getErrors();
				$this->view->addVar('errors', $errors);
				$this->view->display();


					$head = new WarningHeader();
					$state = new WarningState();
					$logger = new WarningLogger();

					$set = array("HTTP Status 400");
					$head->setEntries($set);

					$set = array("Login Failed! An Invalid Set of Data was entered at the Login Page. Pleas ensure the entered information is valid.");
					$state->setEntries($set);
	
					$time = date("h:i:sa");
					$date = date("Y-m-d");
			
					$fulltime = "[ ". $date. " ". $time . " ]";
			
					$set = array($fulltime);
					$logger->setEntries($set);

					$response->create($head, $state, $logger);
					$session->add("RESPONSE", $response);

					
			}
			
			else
			{
				$email = $_POST['LoginEmail'];
				
				$record_array = $this->model->MapLoginPage($email);

				//var_dump($record_array);
			
				if($this->model->verifyPassword($record_array))
				{
					echo "<pre>";
					
					
					$login = $registry->getSession();
					$login->create();
					
						$head = new NoticeHeader();
        				$state = new NoticeState();
        				$logger = new NoticeLogger();

        				$set = array("HTTP Status 202");
        				$head->setEntries($set);

        				$set = array("Login Successful. The information entered was valid and we are now proceeding to Profile Page");
        				$state->setEntries($set);
        
        				$time = date("h:i:sa");
						$date = date("Y-m-d");
			
						$fulltime = "[ ". $date. " ". $time . " ]";
						$set = array($fulltime);
						$logger->setEntries($set);

        				$response->create($head, $state, $logger);
						$login->add("RESPONSE", $response);
					
						$login->add('LoggedIn', $email);

						
						
						echo "heading to login page!";
						header('Location:index.php?controller=profile');
				}
				
				else;
				
			}
			
			$head = new WarningHeader();
        	$state = new WarningState();
			$logger = new WarningLogger();

        	$set = array("HTTP Status 204");
        	$head->setEntries($set);

        	$set = array("No Credentials. The credentials you entered in the Log In Page are valid but are not present in the Database");
        	$state->setEntries($set);
        
        	$time = date("h:i:sa");
			$date = date("Y-m-d");
			
			$fulltime = "[ ". $date. " ". $time . " ]";
			
			$set = array($fulltime);
			$logger->setEntries($set);

        	$response->create($head, $state, $logger);
			$session->add("RESPONSE", $response);

			//echo "Please enter an email and password!";
		}
	}


	public function CreateModel() : ObservableModel
	{
		return new LoginModel();
	}

	public function CreateView() : View
	{
		$view = new View();
		$view->setTemplate(TPL_DIR . '/login.tpl.php');
		return $view;
	}




	public function execute(CommandContext $context) : bool
	{
		$contextData = $context->get('get');
		$newRequest = $contextData['controller'];

	

		$this->run($newRequest);

		return true;
	}

}


