<?php

namespace MOOC\apps;


use MOOC\framework\CommandContext;
use MOOC\framework\PageControllerCommandAbstract;
use MOOC\framework\View;
use MOOC\framework\ObservableModel;
use MOOC\framework\Registry;

use MOOC\framework\NoticeHeader;
use MOOC\framework\NoticeState;
use MOOC\framework\NoticeLogger;
use MOOC\framework\WarningHeader;
use MOOC\framework\WarningState;
use MOOC\framework\WarningLogger;

class ProfileController extends PageControllerCommandAbstract 
{
    public function run(string $request)
    {
        $registry = Registry::instance();
		
        $response = $registry->getHandler();
        $session = $registry->getSession();
        $session->create();
        $user = $session->see('LoggedIn');
        
        $page = 'profile';

        $logcounter = 0;
        
        if ($session->accessible($user, $page))
        {
           // echo "You are allowed to access this page!";
        }

        else
        {   
            $head = new WarningHeader();
            $state = new WarningState();
            $logger = new WarningLogger();

            $set = array("HTTP Status 401");
            $head->setEntries($set);

            $set = array("Unauthorized Access! You have tried accessing the Profile Page without logging in first. Please go to the Log In Page.");
            $state->setEntries($set);
        
            $time = date("h:i:sa");
            $date = date("Y-m-d");

            $fulltime = "[ ". $date. " ". $time . " ]";

            $set = array($fulltime);
            $logger->setEntries($set);

            $response->create($head, $state, $logger);
            
            $session->add("RESPONSE", $response);
            
            $logcounter = 1;

            header('Location:index.php');
        }

        $this->model = $this->CreateModel();

		$this->view = $this->CreateView();

        $this->model->attach($this->view);

        $data = $this->model->MapProfilePage($user);

        $this->model->updateThechangedData($data);

        $this->model->notify();

        if($logcounter == 0)
        {
            $head = new NoticeHeader();
            $state = new NoticeState();
            $logger = new NoticeLogger();

            $set = array("HTTP Status 200");
            $head->setEntries($set);

            $set = array("Authorized Access. The Profile Page was successfully accessed and displayed. Everything is ok.");
            $state->setEntries($set);
        
            $time = date("h:i:sa");
            $date = date("Y-m-d");

            $fulltime = "[ ". $date. " ". $time . " ]";

            $set = array($fulltime);
            $logger->setEntries($set);

            $response->create($head, $state, $logger);
            
            $session->add("RESPONSE", $response);
        }
    }


    public function CreateModel() : ObservableModel
	{
		return new ProfileModel();
	}

	public function CreateView() : View
	{
		$view = new View();
        $view->setTemplate(TPL_DIR . '/profile.tpl.php');
		return $view;
	}
    





    public function execute(CommandContext $context) : bool
    {   
        $contextData = $context->get('get');
        $newRequest = $contextData['controller'];
        
        $this->run($newRequest);
        return true;
    }

}