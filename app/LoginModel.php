<?php

namespace MOOC\apps;

use MOOC\framework\ObservableModel;

use MOOC\framework\StorageAdapter;
use MOOC\framework\LoginMapper;

class LoginModel extends ObservableModel
{


	public function MapLoginPage(string $id)
	{
	
		$setup = array();
		array_push($setup, $id);

		$SetupAdapter = new StorageAdapter($setup);

		$mapper = new LoginMapper($SetupAdapter);

		$ResultAdapter = $mapper->read();

		$array = $ResultAdapter->data;

		//print_r($array);

		return $array;

	}









	public function getAll(): array
	{
		return [];
	}


	//Here getRecord retrieves the user record using the
	//Email Address entered at the Login Page as the parameter
	
	/**
	 * Now that a Data Mapper is being used. This function is no longer used.
	 */
	
	public function getRecord(string $id) : array
	{
		return [];
		
		/*
		var_dump($id);

		$connection = ObservableModel::makeConnection();

		$records = "SELECT * FROM users where email = '$id'";

		$result = $connection->query($records);

		$record = array();

		if($result->num_rows > 0)
		{
			$row = $result->fetch_assoc();
			
				
				echo $row["name"];
				echo "<br>";
				echo $row["email"];
				echo "<br>";
				echo $row["password"];
				echo "<br><br>";
				
				$record = array("email"=> $row["email"], "password"=> $row['password']);
		
		}

		
		
		return $record;
		*/
	}

	
	/**
	 * This function does not interact with the database.
	 * As such it can be left as it is and not interfere with the Data Mapper. 
	 */

	public function verifyPassword(array $pass) : bool
	{

			$hashed = $pass["password"];
			$password = $_POST['LoginPassword'];
			
			//echo $hashed;
			echo "<br>";
			//echo $password;
			echo "<br>";
			
			if(password_verify($password, $hashed))
			{
				//echo "Password is valid!";
				//echo "<br>";echo "<br>";
				return true;
			}

			
			
			return false;
	}
}