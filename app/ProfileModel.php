<?php

namespace MOOC\apps;

use MOOC\framework\ObservableModel;
use MOOC\framework\ReadTrait;

use MOOC\framework\StorageAdapter;
use MOOC\framework\ProfileMapper;


class ProfileModel extends ObservableModel
{

	use ReadTrait;


	public function MapProfilePage(string $id)
	{
		$setup = array();
		array_push($setup, $id);

		$SetupAdapter = new StorageAdapter($setup);

		//var_dump($SetupAdapter);

		$mapper = new ProfileMapper($SetupAdapter);

		$ResultAdapter = $mapper->read();

		$array = $ResultAdapter->data;

		return $array;
	}








	public function getAll(): array
    {
		return [];
    }

    public function getRecord(string $id): array
    {
		return [];
		
		/*
		$data = $this->loadData(DATA_DIR . '/usercourses.json');
		$test = $data['users-courses'];
		
		foreach($test as $key => $value)
		{
			if($value["email"] == $id)
			{
				//echo "Matching Record Found. Displaying Courses";
				
				$user = array();
				$user[0] = $value["course1"];
				$user[1] = $value["course2"];
				$user[2] = $value["course3"];
				$user[3] = $value["course4"];
				
				(
					'name' => $value["name"],
					'email' => $value["email"],
					'course1' => $value["course1"],
					'course2' => $value["course2"],
					'course3' => $value["course3"],
					'course4' => $value["course4"]
				);
				
				
				return ['user'=>$user];
			}
			
		}
		
        return [];*/
	}
	
	public function read(string $id) : array
	{
		return [];
		
		/*
		$loggedEmail = $id;
		
		//var_dump($loggedEmail);


		$connection = ObservableModel::makeConnection();

		$table1 = "select courses.course_id, courses.course_name, courses.course_image, faculty_department.faculty_dept_name, instructors.instructor_name from faculty_department, faculty_dept_courses, courses, course_instructor, instructors where courses.course_id = faculty_dept_courses.course_id AND faculty_dept_courses.faculty_dept_id = faculty_department.faculty_dept_id AND courses.course_id = course_instructor.course_id AND course_instructor.course_id = course_instructor.instructor_id AND course_instructor.instructor_id = instructors.instructor_id";

		$table2 = "SELECT * from user_courses WHERE email='$loggedEmail'";

		$result = $connection->query($table2);
		$studentCourses = [];

		if($result->num_rows > 0)
		{
			
			while($row = $result->fetch_assoc())
			{
				array_push($studentCourses, $row['course_id']);
				
			}
		}
	
		//var_dump($studentCourses);

		$result = $connection->query($table1);

		$courseName = [];
		$courseImage = [];
		$faculty = [];
		$instructor = [];

		if($result->num_rows > 0)
		{
			//$x = 0;
			while($row = $result->fetch_assoc())
			{	
				//echo $row['course_id'];
				//echo "<br>";

				if( 
					($studentCourses[0] == $row['course_id']) ||
					($studentCourses[1] == $row['course_id']) ||
					($studentCourses[2] == $row['course_id']) ||
					($studentCourses[3] == $row['course_id']) 
				)
				{
					//echo "Matching Course Found!";
					//echo "<br>";

					array_push($courseName, $row["course_name"]);
					array_push($courseImage, $row["course_image"]);
					array_push($faculty, $row["faculty_dept_name"]);
					array_push($instructor, $row["instructor_name"]);
					//$x++;
				}	
				
				
			}
		}

		//var_dump($courseImage);

		return 
		[			
			'Name'=>$courseName,
			'Image'=>$courseImage,
			'Faculty'=>$faculty,
			'Instructor'=>$instructor
		];	
		*/	
	}



}