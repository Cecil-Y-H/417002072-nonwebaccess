<?php

namespace MOOC\apps;

use MOOC\framework\CommandContext;
use MOOC\framework\PageControllerCommandAbstract;
use MOOC\framework\View;
use MOOC\framework\ObservableModel;
use MOOC\framework\Registry;


use MOOC\framework\NoticeHeader;
use MOOC\framework\NoticeState;
use MOOC\framework\NoticeLogger;
use MOOC\framework\WarningHeader;
use MOOC\framework\WarningState;
use MOOC\framework\WarningLogger;





class SignupController extends PageControllerCommandAbstract
{
    public function run(string $request)
    {
		$registry = Registry::instance();
		
		$this->model = $this->CreateModel();

		$this->view = $this->CreateView();

		$this->model->attach($this->view);

		$data = $this->model->getAll();

		$this->model->updateThechangedData($data);

		$this->model->notify();
		
		$valid = $_SESSION['valid'];

			$response = $registry->getHandler();
			//$session = SessionClass::getInstance();
			$session = $registry->getSession();
			
			$head = new NoticeHeader();
        	$state = new NoticeState();
        	$logger = new NoticeLogger();

        	$set = array("HTTP Status 200");
        	$head->setEntries($set);

        	$set = array("Page Access OK. The Signup Page was successfully accessed and displayed. Everything is ok.");
        	$state->setEntries($set);
        
        	$time = date("h:i:sa");
			$date = date("Y-m-d");
	
			$fulltime = "[ ". $date. " ". $time . " ]";
	
			$set = array($fulltime);
			$logger->setEntries($set);

        	$response->create($head, $state, $logger);
			$session->add("RESPONSE", $response);

		
		



		if( (!empty($_POST)) && $valid == 1 )
		{
			
			//$validator = Validation::getInstance();
			
			$validator = $registry->getValidation();
			$validator->fillData($_POST);
			$result = $validator->signupValidate();
			
			if(!$result)
			{
				echo "<br>";
				echo "Please see your Errors below!\n";
				$errors = $validator->getErrors();
				$this->view->setTemplate(TPL_DIR . '/signup.tpl.php');
				$this->view->addVar('errors', $errors);
				$this->view->display();

					$head = new WarningHeader();
        			$state = new WarningState();
        			$logger = new WarningLogger();

        			$set = array("HTTP Status 400");
					$head->setEntries($set);

					$set = array("Sign Up Failed! An Invalid Set of Data was entered at the Signup Page. Please ensure the entered information is valid.");
					$state->setEntries($set);
	
					$time = date("h:i:sa");
					$date = date("Y-m-d");
			
					$fulltime = "[ ". $date. " ". $time . " ]";
			
					$set = array($fulltime);
					$logger->setEntries($set);

					$response->create($head, $state, $logger);
					$session->add("RESPONSE", $response);

				
			}
			
			else
			{
					$head = new NoticeHeader();
        			$state = new NoticeState();
        			$logger = new NoticeLogger();

        			$set = array("HTTP Status 201");
        			$head->setEntries($set);

        			$set = array("Sign Up Successful. The information entered was valid and we heading are now proceeding to Login Page");
        			$state->setEntries($set);
        
        			$time = date("h:i:sa");
					$date = date("Y-m-d");
			
					$fulltime = "[ ". $date. " ". $time . " ]";
					$set = array($fulltime);
					$logger->setEntries($set);

        			$response->create($head, $state, $logger);
					$session->add("RESPONSE", $response);
					//$this->model->update($_POST);
					$this->model->MapSignup($_POST);

				header('Location:index.php?controller=login');
			}	
		}
		
	}


	public function CreateModel() : ObservableModel
	{
		return new SignupModel();
	}

	public function CreateView() : View
	{
		$view = new View();
		$view->setTemplate(TPL_DIR . '/signup.tpl.php');
		return $view;
	}



		
	public function execute(CommandContext $context) : bool
	{
		$contextData = $context->get('get');
		$newRequest = $contextData['controller'];

		

		$this->run($newRequest);
		return true;
	}
    

}