<?php

namespace MOOC\apps;

use MOOC\framework\CommandContext;
use MOOC\framework\ObservableModel;
use MOOC\framework\PageControllerCommandAbstract;
use MOOC\framework\View;
use MOOC\framework\Registry;

use MOOC\framework\NoticeHeader;
use MOOC\framework\NoticeState;
use MOOC\framework\NoticeLogger;

class BadRequestController extends PageControllerCommandAbstract 
{
    //Stores context data from the execute method
    //public $contextData = null;

    public function run(string $request)
    { 
        $registry = Registry::instance();


        $this->model = $this->CreateModel();
      
        $this->view = $this->CreateView();

        $this->model->attach($this->view);

        $data = $this->model->getAll();
    
        $this->model->updateThechangedData($data);

        $this->model->notify();

        $response = $registry->getHandler();
        $session = $registry->getSession();

        $head = new NoticeHeader();
        $state = new NoticeState();
        $logger = new NoticeLogger();

        $set = array("HTTP Status 400");
        $head->setEntries($set);

        $set = array("Bad Request! You sent an invalid request using the URL so you were redirected to the Bad Request Page. Please ensure that the ' ?controller= ' part of the URL is valid. ");
        $state->setEntries($set);

        $time = date("h:i:sa");
        $date = date("Y-m-d");

        $fulltime = "[ ". $date. " ". $time . " ]";

        $set = array($fulltime);
        $logger->setEntries($set);
        
        $response->create($head, $state, $logger);
       
        $session->add("RESPONSE", $response);
        
    }

    public function CreateModel() : ObservableModel
    {
        return new IndexModel();
    }

    public function CreateView() : View
    {
        $view = new View();
        $view->setTemplate(TPL_DIR . '/badrequest.tpl.php');
        return $view;
    }




    public function execute(CommandContext $context) : bool
    {   
        $contextData = $context->get('get');
        $newRequest = $contextData['controller'];

        if(empty($newRequest))
        {
            $newRequest = 'index';
        }
       
        
        $this->run($newRequest);
        return true;
    }
}